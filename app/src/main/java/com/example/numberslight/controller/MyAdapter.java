package com.example.numberslight.controller;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.numberslight.model.Item;
import com.example.numberslight.R;
import com.squareup.picasso.Picasso;

import java.util.List;

@SuppressWarnings("ALL")
public class MyAdapter implements ListAdapter {

    final List<Item> itemsList;
    private final Context context;

    public MyAdapter(List<Item> itemsList, Context context) {
        this.itemsList = itemsList;
        this.context = context;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item itemData = itemsList.get(position);
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.listview_item, null);

            TextView name = convertView.findViewById(R.id.list_view_text_view);
            ImageView image = convertView.findViewById(R.id.list_view_image_view);
            name.setText(itemData.getName());
            Picasso.with(context).load(itemData.getImage()).into(image);
        }

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return itemsList.size();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }


}
