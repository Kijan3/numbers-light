package com.example.numberslight.controller;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiServiceFactory {

    private static final String BASE_URL = "http://dev.tapptic.com/test/";
    private final ItemHolder itemHolder;

    public ApiServiceFactory() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        itemHolder = retrofit.create(ItemHolder.class);
    }

    public Call getOneItemCall(String name) {
        return itemHolder.getOneItem(name);
    }

    public Call getItemsListCall() {
        return itemHolder.getItems();
    }

}
