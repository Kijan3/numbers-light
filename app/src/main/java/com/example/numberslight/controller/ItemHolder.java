package com.example.numberslight.controller;

import com.example.numberslight.model.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface ItemHolder {

    @GET("json.php")
    Call<List<Item>> getItems();

    @GET("json.php?name=")
    Call<Item> getOneItem(@Query("name") String name);
}
