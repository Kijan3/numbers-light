package com.example.numberslight;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.numberslight.controller.ApiServiceFactory;
import com.example.numberslight.model.Item;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailScreenActivity extends AppCompatActivity {

    private String itemName;
    private ImageView detailImageView;
    private TextView detailTextView;

    private ApiServiceFactory apiServiceFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);

        detailImageView = findViewById(R.id.detail_image_view);
        detailTextView = findViewById(R.id.detail_text_view);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            itemName = bundle.getString("itemName");
        }

        apiServiceFactory = new ApiServiceFactory();
        getOneItem(itemName);
    }

    @SuppressWarnings("unchecked")
    private void getOneItem(String name) {

        Call call = apiServiceFactory.getOneItemCall(name);

        call.enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if (!response.isSuccessful()) {
                    showAlertDialog("Connection failure - error code: " + response.code());
                    return;
                }

                Item item = response.body();
                Picasso.with(DetailScreenActivity.this).load(Objects.requireNonNull(item).getImage()).into(detailImageView);
                detailTextView.setText(item.getText());
            }

            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                showAlertDialog("Connection failure: " + t.getMessage());
            }
        });
    }

    private void showAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connection");
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getOneItem(itemName);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
