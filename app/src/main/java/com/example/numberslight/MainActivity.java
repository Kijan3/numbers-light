package com.example.numberslight;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.os.Bundle;

import com.example.numberslight.controller.ApiServiceFactory;
import com.example.numberslight.controller.RecyclerViewAdapter;
import com.example.numberslight.model.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener {

    private List<Item> itemsList;
    private ApiServiceFactory apiServiceFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiServiceFactory = new ApiServiceFactory();

        if (!isNetworkConnectionAvailable()) {
            showAlertDialog("No network connection");
        } else {
            getItems();
        }
    }

    @SuppressWarnings("unchecked")
    private void getItems() {

        Call call = apiServiceFactory.getItemsListCall();

        call.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                if (!response.isSuccessful()) {
                    showAlertDialog("Connection failure - error code: " + response.code());
                    return;
                }

                itemsList = response.body();
                createListView(itemsList);

            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                showAlertDialog("Connection failure " + t.getMessage());
            }
        });
    }

    private void createListView(List<Item> itemsList) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(itemsList, this);
        recyclerViewAdapter.setClickListener(this);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void showAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.connection);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.confirm, null);
        builder.setNegativeButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getItems();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean isNetworkConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(MainActivity.this, DetailScreenActivity.class);
        intent.putExtra("itemName", itemsList.get(position).getName());
        startActivity(intent);
    }
}
