package com.example.numberslight.model;

public class Item {

    private String name;
    private String text;
    private String image;

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

}
